package cn.king;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableEurekaClient
@EnableFeignClients //feign处理
public class PageAgentApplication2040 {
    public static void main(String[] args) {
        SpringApplication.run(PageAgentApplication2040.class,args);
    }
}
