package cn.king.config;

import cn.king.util.RabbitMqConstants;
import org.springframework.amqp.core.*;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class RabbitMqConfig {
    //队列
    public static final String QUEUE_PAGE_STATIC = "queue_page_static";
    //交换机名字
    public static final String EXCHANGE_TOPICS_PAGE = RabbitMqConstants.EXCHANGE_TOPICS_PAGE;

    @Value("${rabbitmq.routingKey}")
    private String routingKey;

    /**
     * 交换机配置
     * ExchangeBuilder提供了fanout、direct、topic、header交换机类型的配置
     *
     * @return the exchange
     */
    @Bean(EXCHANGE_TOPICS_PAGE) //spring中bean
    public Exchange EXCHANGE_TOPICS_INFORM() {
        //durable(true)持久化，消息队列重启后交换机仍然存在
        return ExchangeBuilder.topicExchange(EXCHANGE_TOPICS_PAGE).durable(true).build();
    }


    //声明队列
    @Bean(QUEUE_PAGE_STATIC)
    public Queue QUEUE_INFORM_SMS() {
        Queue queue = new Queue(QUEUE_PAGE_STATIC);
        return queue;
    }



    /**
     *
     * @param queue    the queue
     * @param exchange the exchange
     * @return the binding
     */
    @Bean
    public Binding BINDING_QUEUE_INFORM_SMS(@Qualifier(QUEUE_PAGE_STATIC) Queue queue, //通过名字从spring获取bean
                                            @Qualifier(EXCHANGE_TOPICS_PAGE) Exchange exchange) {
        //每个站点的routing可以是不一样的
        System.out.println(routingKey+"jjjjjjjjjjjjjjj");
        return BindingBuilder.bind(queue).to(exchange).with(routingKey).noargs();
    }

}
