package cn.king.service;

import cn.king.domain.Tenant;
import cn.king.query.TenantQuery;
import cn.king.util.PageList;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author yhptest
 * @since 2020-02-15
 */
public interface ITenantService extends IService<Tenant> {

    /**
     * 有关联对象的分页高级查询，必须使用方案2，自定义SQL
     * @param query
     * @return
     */
    PageList<Tenant> selectPageList(TenantQuery query);
}
