package cn.king.service;

import cn.king.domain.Menu;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author yhptest
 * @since 2020-02-15
 */
public interface IMenuService extends IService<Menu> {

}
