package cn.king.service.impl;

import cn.king.domain.Systemdictionary;
import cn.king.mapper.SystemdictionaryMapper;
import cn.king.service.ISystemdictionaryService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author yhptest
 * @since 2020-02-14
 */
@Service
public class SystemdictionaryServiceImpl extends ServiceImpl<SystemdictionaryMapper, Systemdictionary> implements ISystemdictionaryService {

}
