package cn.king.service.impl;

import cn.king.domain.Employee;
import cn.king.domain.Tenant;
import cn.king.mapper.EmployeeMapper;
import cn.king.mapper.TenantMapper;
import cn.king.query.TenantQuery;
import cn.king.service.ITenantService;
import cn.king.util.PageList;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author yhptest
 * @since 2020-02-15
 */
@Service
public class TenantServiceImpl extends ServiceImpl<TenantMapper, Tenant> implements ITenantService {

    @Autowired
    private TenantMapper tenantMapper;

    @Autowired
    private EmployeeMapper employeeMapper;
    @Override
    public PageList<Tenant> selectPageList(TenantQuery query) {
        //构造分页对象
        Page<Tenant> page = new Page<>(query.getPage(),query.getRows());
        //查询当前页数据
        List<Tenant> tenants = tenantMapper.loadPageList(page,query);
        //封装返回对象
        return new PageList<Tenant>(page.getTotal(),tenants);
    }

    @Override
    public boolean deleteById(Serializable id) {
        //除了删除自己还要删除与套餐的中间表
        tenantMapper.deleteById(id);
        //删除中间表
        tenantMapper.deleteTenantMeal(id);
        return true;
    }

    @Override
    public boolean insert(Tenant entity) {

        //保存公司管理员信息-没有租户id
        Employee admin = entity.getAdmin();
        admin.setInputTime(new Date()); //输入时间
        admin.setState(0); // 正常
        admin.setType(1); //是否是租户管理员
        employeeMapper.insert(admin);
        System.out.println(admin.getId()+"iiii");

        //保存公司信息-要返回自增id
        entity.setAdminId(admin.getId());
        tenantMapper.insert(entity);

        System.out.println(entity.getId()+"jjjj");
        //修改管理员的租户id
        admin.setTenantId(entity.getId());
        employeeMapper.updateById(admin);


        //@TODO 如果选择了套餐，还要添加套餐中间表
        return true;
    }


    @Override
    public boolean updateById(Tenant entity) {

        //修改自己信息
        //修改管理员信息
        //修改中间信息
        return super.updateById(entity);
    }
}
