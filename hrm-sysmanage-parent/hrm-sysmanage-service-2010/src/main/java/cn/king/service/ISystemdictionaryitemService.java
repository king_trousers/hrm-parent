package cn.king.service;

import cn.king.domain.Systemdictionaryitem;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author yhptest
 * @since 2020-02-14
 */
public interface ISystemdictionaryitemService extends IService<Systemdictionaryitem> {

}
