package cn.king.service;

import cn.king.domain.Meal;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author yhptest
 * @since 2020-02-15
 */
public interface IMealService extends IService<Meal> {

}
