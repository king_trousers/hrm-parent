king cn.itsource.service;

import cn.itsource.domain.TenantType;
import cn.itsource.query.TenantTypeQuery;
import cn.itsource.util.PageList;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 租户(机构)类型表 服务类
 * </p>
 *
 * @author yhptest
 * @since 2020-02-14
 */
public interface ITenantTypeService extends IService<TenantType> {

    /**
     * 带条件的分页
     * @param query
     * @return
     */
    PageList<TenantType> selectPageLiset(TenantTypeQuery query);
}
