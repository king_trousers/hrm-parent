package cn.king.service.impl;

import cn.king.domain.TenantType;
import cn.king.mapper.TenantTypeMapper;
import cn.king.query.TenantTypeQuery;
import cn.king.service.ITenantTypeService;
import cn.king.util.PageList;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 租户(机构)类型表 服务实现类
 * </p>
 *
 * @author yhptest
 * @since 2020-02-14
 */
@Service
public class TenantTypeServiceImpl extends ServiceImpl<TenantTypeMapper, TenantType> implements ITenantTypeService {

    @Autowired
    private TenantTypeMapper tenantTypeMapper;
    @Override
    public PageList<TenantType> selectPageLiset(TenantTypeQuery query) {
        //作为分页条件传入
        Page<TenantType> page = new Page<TenantType>(query.getPage(),query.getRows());

        //查询得到当前页数据
        List<TenantType> tenantTypes = tenantTypeMapper.loadPageLiset(page, query);

        //分页条件中返回了总数
    //把总数+当前页数据
        return new PageList<TenantType>(page.getTotal(),tenantTypes);
}
}
