package cn.king.mapper;

import cn.king.domain.TenantType;
import cn.king.query.TenantTypeQuery;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.plugins.Page;

import java.util.List;

/**
 * <p>
 * 租户(机构)类型表 Mapper 接口
 * </p>
 *
 * @author yhptest
 * @since 2020-02-14
 */
public interface TenantTypeMapper extends BaseMapper<TenantType> {

    /**
     *
     * @param page 通过page传递分页条件，自动做分页
     * @param query 查询条件，需要自己对查询进行封装
     * @return 通过list返回当前页数据
     */
    List<TenantType> loadPageLiset(Page<TenantType> page, TenantTypeQuery query);
}
