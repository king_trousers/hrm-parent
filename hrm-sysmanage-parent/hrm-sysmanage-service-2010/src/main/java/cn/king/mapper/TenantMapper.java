package cn.king.mapper;

import cn.king.domain.Tenant;
import cn.king.query.TenantQuery;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.plugins.Page;

import java.io.Serializable;
import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author yhptest
 * @since 2020-02-15
 */
public interface TenantMapper extends BaseMapper<Tenant> {


    List<Tenant> loadPageList(Page<Tenant> page, TenantQuery query);

    /**
     * 删除中间表
     * @param id
     */
    void deleteTenantMeal(Serializable id);
}
