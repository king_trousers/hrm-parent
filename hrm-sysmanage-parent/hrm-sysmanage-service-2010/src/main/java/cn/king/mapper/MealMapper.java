package cn.king.mapper;

import cn.king.domain.Meal;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author yhptest
 * @since 2020-02-15
 */
public interface MealMapper extends BaseMapper<Meal> {

}
