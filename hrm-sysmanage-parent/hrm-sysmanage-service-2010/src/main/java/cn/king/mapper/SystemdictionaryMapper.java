package cn.king.mapper;

import cn.king.domain.Systemdictionary;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author yhptest
 * @since 2020-02-14
 */
public interface SystemdictionaryMapper extends BaseMapper<Systemdictionary> {

}
