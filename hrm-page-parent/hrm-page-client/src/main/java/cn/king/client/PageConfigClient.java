package cn.king.client;

import cn.king.util.AjaxResult;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient(value = "HRM-PAGE",fallbackFactory = PageConfigClientFallbackFactory.class  )//服务提供
@RequestMapping("/pageConfig")
public interface PageConfigClient {
    /**
     * 有了pageName就可以获取模块
     * 有了datakey就可以获取数据
     * @param pageName
     * @param dataKey
     * @return
     */
    @PostMapping("/pageStatic")
    AjaxResult staticPage(
            @RequestParam(value = "pageName",required = true) String pageName,
            @RequestParam(value = "dataKey",required = true)String dataKey);
}
