package cn.king.client;

import cn.king.util.AjaxResult;
import feign.hystrix.FallbackFactory;
import org.springframework.stereotype.Component;

@Component
public class PageConfigClientFallbackFactory implements FallbackFactory<PageConfigClient> {
    @Override
    public PageConfigClient create(Throwable throwable) {
        return new PageConfigClient() {
            @Override
            public AjaxResult staticPage(String pageName, String dataKey) {
                return AjaxResult.me().setSuccess(false).setMessage("静态化失败！");
            }
        };
    }
}
