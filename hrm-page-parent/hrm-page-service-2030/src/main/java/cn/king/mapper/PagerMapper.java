package cn.king.mapper;

import cn.king.domain.Pager;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author yhptest
 * @since 2020-02-24
 */
public interface PagerMapper extends BaseMapper<Pager> {

}
