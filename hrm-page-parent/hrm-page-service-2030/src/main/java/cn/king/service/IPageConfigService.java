package cn.king.service;

import cn.king.domain.PageConfig;
import com.baomidou.mybatisplus.service.IService;

import java.io.IOException;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author yhptest
 * @since 2020-02-24
 */
public interface IPageConfigService extends IService<PageConfig> {

    /**
     * 做页面静态化
     * @param pageName
     * @param dataKey
     */
    void staticPage(String pageName, String dataKey) throws IOException;
}
