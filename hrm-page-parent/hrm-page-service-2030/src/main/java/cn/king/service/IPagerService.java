package cn.king.service;

import cn.king.domain.Pager;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author yhptest
 * @since 2020-02-24
 */
public interface IPagerService extends IService<Pager> {

}
