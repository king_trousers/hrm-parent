package cn.king.domain;

import com.baomidou.mybatisplus.enums.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author yhptest
 * @since 2020-02-24
 */
@TableName("t_pager")
public class Pager extends Model<Pager> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;
    /**
     * 英文名
     */
    private String name;
    private String sn;
    /**
     * 别名
     */
    private String alias;
    private Integer type;
    @TableField("site_id")
    private Long siteId;
    private Date createTime;
    /**
     * 是否初始化 0表示没有 1表示已初始化
     */
    private Integer status;
    /**
     * 该页面输出在nginx的路径
     */
    private String physicalPath;
    /**
     * 模板在hdfs中的路径地址 zip
     */
    @TableField("template_url")
    private String templateUrl;
    /**
     * zip包里面要执行某一个模板文件
     */
    private String templateName;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSn() {
        return sn;
    }

    public void setSn(String sn) {
        this.sn = sn;
    }

    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Long getSiteId() {
        return siteId;
    }

    public void setSiteId(Long siteId) {
        this.siteId = siteId;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getPhysicalPath() {
        return physicalPath;
    }

    public void setPhysicalPath(String physicalPath) {
        this.physicalPath = physicalPath;
    }

    public String getTemplateUrl() {
        return templateUrl;
    }

    public void setTemplateUrl(String templateUrl) {
        this.templateUrl = templateUrl;
    }

    public String getTemplateName() {
        return templateName;
    }

    public void setTemplateName(String templateName) {
        this.templateName = templateName;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "Pager{" +
        ", id=" + id +
        ", name=" + name +
        ", sn=" + sn +
        ", alias=" + alias +
        ", type=" + type +
        ", siteId=" + siteId +
        ", createTime=" + createTime +
        ", status=" + status +
        ", physicalPath=" + physicalPath +
        ", templateUrl=" + templateUrl +
        ", templateName=" + templateName +
        "}";
    }
}
