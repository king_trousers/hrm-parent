package cn.king.service;

import cn.king.domain.CourseMarket;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author yhptest
 * @since 2020-02-20
 */
public interface ICourseMarketService extends IService<CourseMarket> {

}
