package cn.king.service.impl;

import cn.king.domain.Course;
import cn.king.domain.CourseDetail;
import cn.king.domain.CourseMarket;
import cn.king.index.doc.EsCourse;
import cn.king.index.repository.EsCourseRepository;
import cn.king.mapper.CourseDetailMapper;
import cn.king.mapper.CourseMapper;
import cn.king.mapper.CourseMarketMapper;
import cn.king.mapper.CourseTypeMapper;
import cn.king.query.CourseQuery;
import cn.king.service.ICourseService;
import cn.king.util.AjaxResult;
import cn.king.util.PageList;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.sort.FieldSortBuilder;
import org.elasticsearch.search.sort.SortBuilders;
import org.elasticsearch.search.sort.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.elasticsearch.core.query.NativeSearchQuery;
import org.springframework.data.elasticsearch.core.query.NativeSearchQueryBuilder;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.math.BigDecimal;
import java.util.*;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author yhptest
 * @since 2020-02-20
 */
@Service
public class CourseServiceImpl extends ServiceImpl<CourseMapper, Course> implements ICourseService {

    @Autowired
    private CourseMapper courseMapper;

    @Autowired
    private CourseDetailMapper courseDetailMapper;
    @Autowired
    private CourseMarketMapper courseMarketMapper;

    @Autowired
    private EsCourseRepository repository;
    @Override
    public PageList<Course> selectpageList(CourseQuery query) {
        Page<Course> page = new Page<>(query.getPage(),query.getRows());
        List<Course> courses = courseMapper.loadPageList(page,query);
        return new PageList<>(page.getTotal(),courses);
    }


    @Override
    public boolean insert(Course entity) {
        //要添加三张表 课程表 详情表 市场信心表
        //tenantId tenantName userId userName
        System.out.println(entity);
        entity.setStatus(0);
        courseMapper.insert(entity);
        Long courseId = entity.getId();
        //同时保存详情和市场信心
        CourseDetail courseDetail = entity.getCourseDetail();
        courseDetail.setCourseId(courseId);
        courseDetailMapper.insert(courseDetail);
        CourseMarket courseMarket = entity.getCourseMarket();
        courseMarket.setCourseId(courseId);
        courseMarketMapper.insert(courseMarket);
        return true;
    }

    @Override
    public boolean updateById(Course entity) {
        //@TODO 修改也要同步修改多个表和索引库，删除也要同步删除多个表和索引库，要通过更新静态化页面
        return super.updateById(entity);
    }



    @Override
    public AjaxResult onLine(Long[] ids) {

        try
        {
            //1添加索引库
            // 通过id查询数据库里面课程
            List<Course> courses = courseMapper
                    .selectBatchIds(Arrays.asList(ids));
            //转换为doc
            List<EsCourse> esCourses = courses2EsCourses(courses);
            //批量添加就ok
            repository.saveAll(esCourses);
            //2 修改数据库状态和上线时间 - ids,time
            Map<String,Object> params = new HashMap<>();
            params.put("ids",ids);
            params.put("onLineTime",new Date());
            // 修改状态和上线时间
            //update t_couse set status=1 and start_time={} where id in (1,2,3,4)
            courseMapper.onLine(params);
            //@TODO 课程详情页静态化处理
            return AjaxResult.me();
        }catch (Exception e){
            e.printStackTrace();
            return AjaxResult.me().setSuccess(false).setMessage("上线失败！"+e.getMessage());
        }


    }


    @Override
    public AjaxResult offLine(Long[] ids) {
        try
        {
            //1 删除索引库里面的内容
            //        List<Course> courses = courseMapper
            //                .selectBatchIds(Arrays.asList(ids));
            //转换为doc
            //        List<EsCourse> esCourses = courses2EsCourses(courses);
            //        repository.deleteAll(esCourses);
            for (Long id : ids) {
                repository.deleteById(id);
            }
            //2 修改数据库状态 status=0 end_time
            Map<String,Object> params = new HashMap<>();
            params.put("ids",ids);
            params.put("offLineTime",new Date());
            // 修改状态和下线时间
            //update t_couse set status=0 and start_time={} where id in (1,2,3,4)
            courseMapper.offLine(params);
            //@TODO 要删除静态化页面
            return AjaxResult.me();
        }catch (Exception e){
            e.printStackTrace();
            return AjaxResult.me().setSuccess(false).setMessage("下线失败！"+e.getMessage());
        }

    }

    //es dsl

    /**
     *GET crm/user/_search
     * {
     *     "query": {
     *        "bool": {
     *               "must": [
     *                                        {"match": {"description": "search" }}
     *                ],
     *                "filter": {
     *                    "term": {"tags": "lucene"}
     *                }
     *         }
     *     },
     *     "from": 20,
     *     "size": 10,
     *     "_source": ["fullName", "age", "email"],
     *     "sort": [{"join_date": "desc"},{"age": "asc"}]
     * }
     * @param query
     * @return
     */
    @Override
    public PageList<EsCourse> queryCourses(CourseQuery query) {

        NativeSearchQueryBuilder builder = new NativeSearchQueryBuilder();
        //1条件 query bool
        BoolQueryBuilder boolQuery = QueryBuilders.boolQuery();
        if (StringUtils.hasLength(query.getKeyword())) {
            //must
            boolQuery.must(QueryBuilders.matchQuery("all",query.getKeyword()));
        }
        //filter
        List<QueryBuilder> filter = boolQuery.filter();
        if (query.getCourseType()!=null){
            //类型过滤
            filter.add(QueryBuilders.termQuery("courseTypeId",query.getCourseType()));
        }
        if (query.getPriceMin()!=null&&query.getPriceMax()!=null){
            //区间过滤
            filter.add(QueryBuilders.rangeQuery("price").gte(query.getPriceMin()).lte(query.getPriceMax()));
        }
        builder.withQuery(boolQuery);
        //2 排序 jg desc
        SortOrder defaultSortOrder = SortOrder.DESC;
        //先给默认值，如果不符合在修改它
        if (query.getSortType() != null && !query.getSortType().equals("desc")) {
            defaultSortOrder = SortOrder.ASC;
        }
        //有可能有多种排序方式，现在使用的能测试的是价格
        if (StringUtils.hasLength(query.getSortField())&&query.getSortField().equals("jg")){
            FieldSortBuilder order = SortBuilders.fieldSort("price").order(defaultSortOrder);
            builder.withSort(order);
        }
        //3 分页 从零开始-1
        builder.withPageable(PageRequest.of(query.getPage()-1,query.getRows()));
        //4 截取字段 先不多
        //5 查询封装
        NativeSearchQuery esQuery = builder.build();
        org.springframework.data.domain.Page<EsCourse> page =
                repository.search(esQuery);
        return new PageList<>(page.getTotalElements(),page.getContent());
    }

    /**
     * 进行转换分为三个步骤来做：
     *   1 声明要返回
     *   2 转换
     *   3返回
     * @param courses
     * @return
     */
    private List<EsCourse> courses2EsCourses(List<Course> courses) {
        //1 声明要返回
        List<EsCourse> result = new ArrayList<>();
        //2 转换
        for (Course course : courses) {
            result.add(course2EsCourse(course));
        }
        //3返回
        return  result;
    }

    @Autowired
    private CourseTypeMapper courseTypeMapper;
    //把一个course转换为EsCourse
    private EsCourse course2EsCourse(Course course) {
        //1 声明要返回
        EsCourse result = new EsCourse();
        //  2 转换
        result.setId(course.getId());
        result.setName(course.getName());
        result.setUsers(course.getUsers());

        result.setCourseTypeId(course.getCourseTypeId());
        //type-同库-没有做关联查询
        if (course.getCourseType() != null) {
            String typeName = courseTypeMapper.selectById(course.getCourseType()).getName();
            result.setCourseTypeName(typeName);
        }
        result.setGradeId(course.getGrade());
        result.setGradeName(course.getGradeName());
        result.setStatus(course.getStatus());
        result.setTenantId(course.getTenantId());
        result.setTenantName(course.getTenantName());
        result.setUserId(course.getUserId());
        result.setUserName(course.getUserName());
        result.setStartTime(course.getStartTime());
        result.setEndTime(course.getEndTime());
        //Detail 查询是在all
        List<CourseDetail> courseDetails = courseDetailMapper.selectList(new EntityWrapper<CourseDetail>()
                .eq("course_id", course.getId()));
        if (courseDetails!=null && courseDetails.size()>0){
            result.setIntro(courseDetails.get(0).getIntro());
        }
        //resource
        result.setResources(course.getPic());
        //market
        List<CourseMarket> courseMarkets = courseMarketMapper.selectList(new EntityWrapper<CourseMarket>()
                .eq("course_id", course.getId()));
        if (courseMarkets!=null && courseMarkets.size()>0){
            CourseMarket courseMarket = courseMarkets.get(0);
            result.setExpires(courseMarket.getExpires());
            result.setPrice(new BigDecimal(courseMarket.getPrice()));
            result.setPriceOld(new BigDecimal(courseMarket.getPriceOld()));
            result.setQq(courseMarket.getQq());
        }
        //   3返回
        return result;
    }

}
