package cn.king.service;

import cn.king.domain.Course;
import cn.king.query.CourseQuery;
import cn.king.util.AjaxResult;
import cn.king.util.PageList;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author yhptest
 * @since 2020-02-20
 */
public interface ICourseService extends IService<Course> {

    /**
     * 分页+高级查询+跨表查询
     * @param query
     * @return
     */
    PageList<Course> selectpageList(CourseQuery query);

    AjaxResult onLine(Long[] ids);

    AjaxResult offLine(Long[] ids);
}
