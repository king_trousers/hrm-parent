package cn.king.service;

import cn.king.domain.CourseType;
import com.baomidou.mybatisplus.service.IService;

import java.util.List;

/**
 * <p>
 * 课程目录 服务类
 * </p>
 *
 * @author yhptest
 * @since 2020-02-17
 */
public interface ICourseTypeService extends IService<CourseType> {

    /**
     * 获取pid的子子孙孙
     * @param pid
     * @return
     */
    List<CourseType> treeData(long pid);
}
