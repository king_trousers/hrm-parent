package cn.king.service.impl;

import cn.king.domain.CourseDetail;
import cn.king.mapper.CourseDetailMapper;
import cn.king.service.ICourseDetailService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author yhptest
 * @since 2020-02-20
 */
@Service
public class CourseDetailServiceImpl extends ServiceImpl<CourseDetailMapper, CourseDetail> implements ICourseDetailService {

}
