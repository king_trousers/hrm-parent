package cn.king.service.impl;

import cn.king.domain.CourseMarket;
import cn.king.mapper.CourseMarketMapper;
import cn.king.service.ICourseMarketService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author yhptest
 * @since 2020-02-20
 */
@Service
public class CourseMarketServiceImpl extends ServiceImpl<CourseMarketMapper, CourseMarket> implements ICourseMarketService {

}
