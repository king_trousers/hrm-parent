package cn.king.mapper;

import cn.king.domain.CourseResource;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author yhptest
 * @since 2020-02-20
 */
public interface CourseResourceMapper extends BaseMapper<CourseResource> {

}
