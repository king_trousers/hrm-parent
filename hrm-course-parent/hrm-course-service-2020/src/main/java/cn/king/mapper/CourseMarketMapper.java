package cn.king.mapper;

import cn.king.domain.CourseMarket;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author yhptest
 * @since 2020-02-20
 */
public interface CourseMarketMapper extends BaseMapper<CourseMarket> {

}
