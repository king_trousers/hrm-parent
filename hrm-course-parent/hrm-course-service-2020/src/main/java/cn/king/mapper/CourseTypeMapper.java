package cn.king.mapper;

import cn.king.domain.CourseType;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 课程目录 Mapper 接口
 * </p>
 *
 * @author yhptest
 * @since 2020-02-17
 */
public interface CourseTypeMapper extends BaseMapper<CourseType> {

}
