package cn.king.cache;

import cn.king.domain.CourseType;

import java.util.List;

public interface ICourseTypeCache {
    /**
     * 获取courseType的缓存
     * @return
     */
    List<CourseType> getTreeData();

    /**
     * 设置courseType到缓存中
     * @param courseTypesOfDb
     */
    void setTreeData(List<CourseType> courseTypesOfDb);
}
