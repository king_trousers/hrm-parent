package cn.king.cache.impl;

import cn.king.cache.ICourseTypeCache;
import cn.king.client.PageConfigClientFallbackFactory;
import cn.king.client.RedisClient;
import cn.king.domain.CourseType;
import com.alibaba.fastjson.JSONArray;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class CourseTypeCacheImpl implements ICourseTypeCache {

    //获取和设置都要使用
    private final static String COURSETYPE_KEY_IN_CACHE
            = "courseType_Key_in_cache";
    @Autowired
    private RedisClient redisClient;
    @Override
    public List<CourseType> getTreeData() {
        //获取的是字符串
        String treeDataOfCache = redisClient.get(COURSETYPE_KEY_IN_CACHE);
        //转换为java对象
        return JSONArray.parseArray(treeDataOfCache,CourseType.class);
    }

    @Override
    public void setTreeData(List<CourseType> courseTypesOfDb) {

        if (courseTypesOfDb == null || courseTypesOfDb.size()<1){
            //缓存穿透
            redisClient.add(COURSETYPE_KEY_IN_CACHE,"[]");
        }else{
            //转换为json字符串
            String jsonArrStr = JSONArray.toJSONString(courseTypesOfDb);
            //设置获取
            //缓存数据永远不过期 缓存击穿 缓存雪崩
            redisClient.add(COURSETYPE_KEY_IN_CACHE,jsonArrStr);
        }

    }
}
