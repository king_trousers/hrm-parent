package cn.king.controller;

import cn.king.index.doc.EsCourse;
import cn.king.service.ICourseService;
import cn.king.domain.Course;
import cn.king.query.CourseQuery;
import cn.king.util.AjaxResult;
import cn.king.util.PageList;
import com.baomidou.mybatisplus.plugins.Page;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/course")
public class CourseController {
    @Autowired
    public ICourseService courseService;
    @PostMapping("queryCourses") //Map
    public PageList<EsCourse> add(@RequestBody CourseQuery query){
        return courseService.queryCourses(query);
    }


    /**
    * 保存和修改公用的
    * @param course  传递的实体
    * @return Ajaxresult转换结果
    */
	@PostMapping
    public AjaxResult add(@RequestBody Course course){
        try {
			courseService.insert(course);
            return AjaxResult.me();
        } catch (Exception e) {
            e.printStackTrace();
            return AjaxResult.me().setMessage("保存对象失败！"+e.getMessage());
        }
    }

    /**
    * 删除对象信息
    * @param id
    * @return
    */
    @DeleteMapping(value="/{id}")
    public AjaxResult delete(@PathVariable("id") Long id){
        try {
            courseService.deleteById(id);
            return AjaxResult.me();
        } catch (Exception e) {
        e.printStackTrace();
            return AjaxResult.me().setMessage("删除对象失败！"+e.getMessage());
        }
    }

 /**
    * 保存和修改公用的
    * @param course  传递的实体
    * @return Ajaxresult转换结果
    */
	@PutMapping
    public AjaxResult addOrUpdate(@RequestBody Course course){
        try {
            // @TODO 以后登录成功都能获取,现在使用holder来模拟
            //登录成功后设置到UserInfoHolder，以后所有模块要使用都直接使用UserInfoHolder
//            course.setTenantId(UserInfoHolder.getTenant().getId());
//            course.setTenantName(UserInfoHolder.getTenant().getCompanyName());
//            course.setUserId(UserInfoHolder.getLoginUser().getId());
//            course.setUserName(UserInfoHolder.getLoginUser().getUsername());
            if(course.getId()!=null){
                courseService.updateById(course);
            }else{
                courseService.insert(course);
            }

            return AjaxResult.me();
        } catch (Exception e) {
            e.printStackTrace();
            return AjaxResult.me().setMessage("保存对象失败！"+e.getMessage());
        }
    }
	
    //获取用户
    @GetMapping("/{id}")
    public Course get(@PathVariable("id")Long id)
    {
        return courseService.selectById(id);
    }


    /**
    * 查看所有的员工信息
    * @return
    */
    @GetMapping
    public List<Course> list(){

        return courseService.selectList(null);
    }


    /**
    * 分页查询数据
    *
    * @param query 查询对象
    * @return PageList 分页对象
    */
    @PostMapping("/list")
    public PageList<Course> json(@RequestBody CourseQuery query)
    {
        return courseService.selectpageList(query);
    }


    //上线
    //参数，long[] 支持批量上线
    //返回值：Ajaxresult只关心成功与否
    @PostMapping("/onLine")
    public AjaxResult onLine(@RequestBody Long[] ids){
        return courseService.onLine(ids);
    }
    //下线
    @PostMapping("/offLine")
    public AjaxResult offLine(@RequestBody Long[] ids){

        return courseService.offLine(ids);
    }
}
