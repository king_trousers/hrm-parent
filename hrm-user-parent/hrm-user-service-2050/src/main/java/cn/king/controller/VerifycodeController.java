package cn.king.controller;

import cn.king.service.IVerifycodeService;
import cn.king.util.AjaxResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

/**
 * 验证码的类型
 *    图像验证码
 *    短信验证
 */
@RestController
@RequestMapping("/verifycode")
public class VerifycodeController {
    @Autowired
    private IVerifycodeService verifycodeService;
    /**
     *
     * @return base64图片值
     */
    @PostMapping("/imageCode/{imageCodeKey}")
    public String imageCode(@PathVariable("imageCodeKey") String imageCodeKey)
    {
       return verifycodeService.getImageCode(imageCodeKey);
    }
    @PostMapping("/sendSmsCode")
    public AjaxResult sendSmsCode(@RequestBody Map<String,String> params)
    {
        //发送短信验证码到那个手机
//        mobile: this.formParams.mobile,
//                //图片验证码信息要发过去 后台要校验图片验证
//                imageCode:this.formParams.imageCode,
//            imageCodeKey:localStorage.getItem("imageCodeKey")
           return verifycodeService.sendSmsCode(params);

    }


}
