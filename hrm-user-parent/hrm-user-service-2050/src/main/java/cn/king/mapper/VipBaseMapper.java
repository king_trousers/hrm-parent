package cn.king.mapper;

import cn.king.domain.VipBase;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 会员基本信息 Mapper 接口
 * </p>
 *
 * @author yhptest
 * @since 2020-02-27
 */
public interface VipBaseMapper extends BaseMapper<VipBase> {

}
