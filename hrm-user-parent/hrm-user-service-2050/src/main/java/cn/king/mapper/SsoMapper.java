package cn.king.mapper;

import cn.king.domain.Sso;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 会员登录账号 Mapper 接口
 * </p>
 *
 * @author yhptest
 * @since 2020-02-27
 */
public interface SsoMapper extends BaseMapper<Sso> {

}
