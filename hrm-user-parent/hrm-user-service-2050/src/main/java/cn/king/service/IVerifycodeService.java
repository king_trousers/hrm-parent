package cn.king.service;

import cn.king.util.AjaxResult;

import java.util.Map;

public interface IVerifycodeService {
    /**
     * 获取验证码
     * @param imageCodeKey
     * @return
     */
    String getImageCode(String imageCodeKey);

    /**
     * 发送短信验证码
     * @param params
     * @return
     */
    AjaxResult sendSmsCode(Map<String, String> params);
}
