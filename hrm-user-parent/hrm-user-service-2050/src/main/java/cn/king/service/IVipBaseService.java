package cn.king.service;

import cn.king.domain.VipBase;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 会员基本信息 服务类
 * </p>
 *
 * @author yhptest
 * @since 2020-02-27
 */
public interface IVipBaseService extends IService<VipBase> {

}
