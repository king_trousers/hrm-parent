package cn.king.service;

import cn.king.domain.Sso;
import cn.king.util.AjaxResult;
import com.baomidou.mybatisplus.service.IService;

import java.util.Map;

/**
 * <p>
 * 会员登录账号 服务类
 * </p>
 *
 * @author yhptest
 * @since 2020-02-27
 */
public interface ISsoService extends IService<Sso> {

    AjaxResult register(Map<String, String> params);

    AjaxResult login(Sso sso);

    Sso querySso(String accessToken);
}
