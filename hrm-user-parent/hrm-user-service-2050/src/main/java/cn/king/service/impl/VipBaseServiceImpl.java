package cn.king.service.impl;

import cn.king.domain.VipBase;
import cn.king.mapper.VipBaseMapper;
import cn.king.service.IVipBaseService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 会员基本信息 服务实现类
 * </p>
 *
 * @author yhptest
 * @since 2020-02-27
 */
@Service
public class VipBaseServiceImpl extends ServiceImpl<VipBaseMapper, VipBase> implements IVipBaseService {

}
