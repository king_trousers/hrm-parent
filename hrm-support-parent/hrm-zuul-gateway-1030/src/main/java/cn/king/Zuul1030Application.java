package cn.king;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;

@SpringBootApplication
@EnableZuulProxy
public class Zuul1030Application {
    public static void main(String[] args) {
        SpringApplication.run(Zuul1030Application.class,args);
    }
}
